#include <iostream>
#include <windows.h>
#include <psapi.h> // For access to GetModuleFileNameEx
#include <tchar.h>
#include <windef.h>

using namespace std;

string GetProcessPathByID(DWORD id)
{
	HANDLE processHandle = NULL;
	TCHAR filename[MAX_PATH];
	processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, id);
	if (processHandle != NULL) {
		if (GetModuleFileNameEx(processHandle, NULL, filename, MAX_PATH) == 0) {
			return "Failed to get module filename.";
		}
		else {
			std::wstring arr_w(filename);
			std::string arr_s(arr_w.begin(), arr_w.end());
			return arr_s;
		}
		CloseHandle(processHandle);
	}
	else {
		return "Failed to open process.";
	}
}

string GetActiveWindowTitle()
{
	char wnd_title[256];
	HWND hwnd = GetForegroundWindow(); // get handle of currently active window
	GetWindowTextA(hwnd, wnd_title, sizeof(wnd_title));
	return wnd_title;
}

int main()
{
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	DWORD id;
	GetWindowThreadProcessId(GetForegroundWindow(), &id);
	string prev, current;
	while (true)
	{
		GetWindowThreadProcessId(GetForegroundWindow(), &id);
		current = GetProcessPathByID(id);
		if (prev.find("\\SearchUI.exe") != string::npos && current.find("\\ApplicationFrameHost.exe") != string::npos && GetActiveWindowTitle().find("Microsoft Edge") != string::npos)
		{
			STARTUPINFO sj;
			PROCESS_INFORMATION pj;
			ZeroMemory(&sj, sizeof(sj));
			sj.cb = sizeof(sj);
			ZeroMemory(&pj, sizeof(pj));
			if (CreateProcess(L".\\EdgeAutomation.exe", NULL, NULL, NULL, FALSE, 0, NULL, NULL, &sj, &pj))
			{
				WaitForSingleObject(pj.hProcess, INFINITE);
				CloseHandle(pj.hProcess);
				CloseHandle(pj.hThread);
			}
		}
		GetWindowThreadProcessId(GetForegroundWindow(), &id);
		prev = GetProcessPathByID(id);
		Sleep(100);
	}
	system("pause");
	return 0;
}