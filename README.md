Cortana search using default web browser
----------------------------------------

Restores back functionality recently removed by Microsoft, that allowed for Cortana searches to be opened in the default web browser registered on the system, instead of Microsoft Edge only.
This application is coded in C++ and C#.

This is FOSS.