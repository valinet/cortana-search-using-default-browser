﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Diagnostics;
using System.Management.Instrumentation;
using System.Management;
using System.Windows.Forms;
using System.Threading;

namespace EdgeAutomation
{
    class Program
    {
        [DllImport("user32")]
        public static extern IntPtr GetDesktopWindow();
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetWindowTextLength(IntPtr hWnd);
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);
        enum ShowWindowCommands
        {
            /// <summary>
            /// Hides the window and activates another window.
            /// </summary>
            Hide = 0,
            /// <summary>
            /// Activates and displays a window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position.
            /// An application should specify this flag when displaying the window 
            /// for the first time.
            /// </summary>
            Normal = 1,
            /// <summary>
            /// Activates the window and displays it as a minimized window.
            /// </summary>
            ShowMinimized = 2,
            /// <summary>
            /// Maximizes the specified window.
            /// </summary>
            Maximize = 3, // is this the right value?
                          /// <summary>
                          /// Activates the window and displays it as a maximized window.
                          /// </summary>       
            ShowMaximized = 3,
            /// <summary>
            /// Displays a window in its most recent size and position. This value 
            /// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except 
            /// the window is not activated.
            /// </summary>
            ShowNoActivate = 4,
            /// <summary>
            /// Activates the window and displays it in its current size and position. 
            /// </summary>
            Show = 5,
            /// <summary>
            /// Minimizes the specified window and activates the next top-level 
            /// window in the Z order.
            /// </summary>
            Minimize = 6,
            /// <summary>
            /// Displays the window as a minimized window. This value is similar to
            /// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowMinNoActive = 7,
            /// <summary>
            /// Displays the window in its current size and position. This value is 
            /// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the 
            /// window is not activated.
            /// </summary>
            ShowNA = 8,
            /// <summary>
            /// Activates and displays the window. If the window is minimized or 
            /// maximized, the system restores it to its original size and position. 
            /// An application should specify this flag when restoring a minimized window.
            /// </summary>
            Restore = 9,
            /// <summary>
            /// Sets the show state based on the SW_* value specified in the 
            /// STARTUPINFO structure passed to the CreateProcess function by the 
            /// program that started the application.
            /// </summary>
            ShowDefault = 10,
            /// <summary>
            ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
            /// that owns the window is not responding. This flag should only be 
            /// used when minimizing windows from a different thread.
            /// </summary>
            ForceMinimize = 11
        }
        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, long dwNewLong);
        enum WindowLongFlags : int
        {
            GWL_EXSTYLE = -20,
            GWLP_HINSTANCE = -6,
            GWLP_HWNDPARENT = -8,
            GWL_ID = -12,
            GWL_STYLE = -16,
            GWL_USERDATA = -21,
            GWL_WNDPROC = -4,
            DWLP_USER = 0x8,
            DWLP_MSGRESULT = 0x0,
            DWLP_DLGPROC = 0x4
        }
        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("psapi.dll")]
        public static extern bool EmptyWorkingSet(IntPtr hProcess);
        public static string GetText(IntPtr hWnd)
        {
            // Allocate correct string length first
            int length = GetWindowTextLength(hWnd);
            StringBuilder sb = new StringBuilder(length + 1);
            GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        static void Main(string[] args)
        {
            ShowWindow(Process.GetCurrentProcess().MainWindowHandle, ShowWindowCommands.Hide);
            /*string prev, current;
            uint id;
            GetWindowThreadProcessId(GetForegroundWindow(), out id);
            prev = Process.GetProcessById((int)id).MainModule.FileName;
            EventWaitHandle waithandler = new EventWaitHandle(false, EventResetMode.AutoReset, Guid.NewGuid().ToString()); do
            {
                try
                {
                    GetWindowThreadProcessId(GetForegroundWindow(), out id);
                    current = Process.GetProcessById((int)id).MainModule.FileName;
                    if (prev.Contains("\\SearchUI.exe") && current.Contains("\\ApplicationFrameHost.exe") && GetText(GetForegroundWindow()).ToString().Contains("Microsoft Edge"))
                    {
                        //int extendedStyle = GetWindowLong(GetForegroundWindow(), -20);
                        //SetWindowLong(GetForegroundWindow(), -20, extendedStyle | 0x00000020L);*/
                        begin:

                        var children = AutomationElement.RootElement.FindAll(TreeScope.Children, Condition.TrueCondition);
                        var url = "{?}";

                        foreach (AutomationElement child in children)
                        {
                            if (child.Current.ClassName.Equals("ApplicationFrameWindow"))
                            {
                                var deeper = child.FindAll(TreeScope.Children, Condition.TrueCondition);
                                foreach (AutomationElement d in deeper)
                                {
                                    if (d.Current.Name.Equals("Microsoft Edge"))
                                    {
                                        bool detected = false;
                                        try
                                        {
                                            var deeper2 = d.FindAll(TreeScope.Children, Condition.TrueCondition);
                                            foreach (AutomationElement d2 in deeper2)
                                            {
                                                if (d2.Current.AutomationId.Equals("addressEditBox"))
                                                {
                                                    var value = d2.GetCurrentPropertyValue(AutomationElement.NameProperty) as string;
                                                    value = ((TextPattern)d2.GetCurrentPattern(TextPattern.Pattern)).DocumentRange.GetText(Int32.MaxValue);
                                                    url = value;
                                                    if (url.Contains("bing."))
                                                    {
                                                        SendKeys.SendWait("^w");
                                                        Process.Start("https://" + url);
                                                        System.Threading.Thread.Sleep(1000);
                                                    }
                                                    Environment.Exit(0);
                                                    detected = true;
                                                }
                                            }
                                            if (!detected) goto begin;
                                        }
                                        catch { }
                                    }

                                }
                            }
                        }

            Environment.Exit(0);

            /*}
            GetWindowThreadProcessId(GetForegroundWindow(), out id);
            prev = Process.GetProcessById((int)id).MainModule.FileName;
        }
        catch { }
        waithandler.WaitOne(TimeSpan.FromMilliseconds(500));
        GC.Collect();
        Process pProcess = Process.GetCurrentProcess();
        bool bRes = EmptyWorkingSet(pProcess.Handle);
    } while (true);*/
        }
    }
}
